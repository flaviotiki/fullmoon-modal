Application.Component.Modal = Class.extend({

    init: function (settings) {
        this.settings = settings;

        this.setup();
        this.bind();
    },

    setup: function () {

        this.settings = jQuery.extend({
            aim: '.viral',
            purport: 'video',
            purportDest: '.modal-view iframe.yt-video',
            lnk: 'url',
            lnkDest: '.modal-view .comment .button a',
            tit: 'title',
            titDest: '.modal-view .title',
            modal: '.modal-gallery',
            goleft: '.modal-controls .go-left',
            goright: '.modal-controls .go-right',
            argument: 'index'
        }, this.settings);

    },

    bind: function () {

        var _this = this;

        $(_this.settings.aim).on('click', function(e) {
            e.preventDefault();
            var index = $(this).index();
            $(_this.settings.modal).toggleClass('open');
            _this.build(index);
        });

        $(_this.settings.goleft + ', ' + _this.settings.goright).on('click', function(e) {
            e.preventDefault();
            _this.build($(this).data(_this.settings.argument));
        });

    },

    build: function (index) {

        var _this = this;
        var _item = $(_this.settings.aim).eq(index);

        $(_this.settings.aim).removeClass('active');
        _item.addClass('active');

        $(_this.settings.purportDest).attr('src', _item.data(_this.settings.purport));
        $(_this.settings.lnkDest).attr('href', _item.data(_this.settings.lnk));
        $(_this.settings.titDest).text(_item.data(_this.settings.tit));

        var totalElement =  $(_this.settings.aim).length;
        var indexLeft = index - 1;
        var indexRight = index + 1;

        if (index == 0) {
            indexLeft = totalElement - 1;
        }

        if (index >= totalElement - 1) {
            indexRight = 0;
        }

        $(_this.settings.goleft).data(_this.settings.argument, indexLeft);
        $(_this.settings.goright).data(_this.settings.argument, indexRight);
    }

});